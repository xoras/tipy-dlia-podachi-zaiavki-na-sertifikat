# Типы для подачи заявки на сертификат

## Классы

Экземпляр класса представляет из себя объект JSON с набором полей согласно описанию.

### Organization

- `inn` <[string]> ИНН.
- `kpp` <[string]> КПП.
- `nameshort` <[string]> Краткое наименование.
- `namelong` <[string]> Полное наименование.
- `ogrn` <[string]> ОГРН.
- `pfr` <[string]> Регистрационный номер ПФР.
- `fss` <[string]> Регистрационный номер ФСС.
- `note` <[string]> Примечание.
- `ongk` <[string]> Строка "n" (латиница, нижний регистр).
- `gkname` <[string]> Пустая строка.
- `gkinn` <[string]> Пустая строка.
- `gkkpp` <[string]> Пустая строка.
- `gkproducttype` <[string]> Строка "АО" (кириллица, верхний регистр).
- `uraddress` <[string]> Юридический адрес.
- `factaddress` <[string]> Фактический адрес.
- `email` <[string]> email.
- `nalogsystem` <[string]> Строка "ОСНО" (кириллица, верхний регистр).
- `nalogsystem` <[Array]<[Signatory]>> Подписанты (достаточно генерального директора).

Пример:
```json
{
	"inn": "7816655847", 
    "kpp": "781001001",
	"nameshort": "ООО \"Вектор\"",
    "namelong": "ООО \"Вектор\"",
    "ogrn": "1167847257973",    
    "pfr": "088009116255",
	"fss": "7815047408",
    "note": "",
    "ongk": "n",
    "gkname": "",
    "gkinn": "",
    "gkkpp": "",
    "gkproducttype": "АО",
    "uraddress": "196247, Санкт-Петербург г, Кубинская ул, д. 34, лит. А, пом. 1-Н",
    "factaddress": "196247, Санкт-Петербург г, Кубинская ул, д. 34, лит. А, пом. 1-Н",
    "email": "svektor2016@gmail.com",
	"nalogsystem": "ОСНО",
    "signatories": [...]
}
```

### Signatory

- `f` <[string]> Фамилия.
- `i` <[string]> Имя.
- `o` <[string]> Отчество.
- `sex` <[string]> Пол "М" или "Ж" (кириллица, верхний регистр).
- `dateofb` <[string]> Дата рождения (строка формата "ДДММГГГГ").
- `placeofb` <[string]> Место рождения.
- `citizenship` <[string]> Гражданство. В формате значения Альфа-3 из Общероссийского классификатора стран мира).
- `snils` <[string]> СНИЛС.
- `paspseries` <[string]> Серия паспорта.
- `paspnumber` <[string]> Номер паспорта.
- `paspdate` <[string]> Дата выдачи паспорта (строка формата "ДДММГГГГ").
- `paspunitcode` <[string]> Код подразделения выдавшего паспорт.

Пример:
```json
{
	"f": "Степанов",
	"i": "Юрий",
	"o": "Иванович",
	"sex": "М",
	"dateofb": "25121972",
	"placeofb": "Ленинград",
	"citizenship": "RUS",
	"snils": "01131267482",
	"paspseries": "4112",
	"paspnumber": "489659",
	"paspdate": "20122012",
	"paspunitcode": "654654"
}
```

[Array]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array "Array"
[boolean]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#Boolean_type "Boolean"
[Object]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object "Object"
[string]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures#String_type "String"
[Organization]: #class-page "Organization"
[Signatory]: #class-page "Signatory"